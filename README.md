
## Creación de archivos

  Para empezar hay que crear carpeta "repositorio";dentro del repositorio creamos los archivos "index.html" y "index.js" para dspues publicarlo.

  
  ## Una vez dentro del repositorio se añade el proyecto a git usando el siguiente comando

  `$ git init`

  
## Se introduce el correo y despues el nombre del usuario de gitlab; que en este caso tanto el correo y el nombre del usuario son los mismos tanto para github como para gitlab
	## Comando para el correo
   `$ git config --global user.email "gmail@email.com"` 
    	## Comando para elnombre del usuario
  `$ git config --global user.name "usuario"`


  ## Para saber si se ha creado correctamente utilizaremos el siguiente comando que comprueba el estado.
  

  `$ git status`

## Despues de la comprobación del estado hay que añadir los archivos que queramos.
 
 `git add index.html index.js`

## Se tienen que registrar los cambios 

`git commit -m "Comentario"`


## Volvemos a comprobar que todo esta correctamente sin ningún error.
`$ git status`


## Introducimos el siguiente comando para verificar los cambios
`$ git remote add origin https://gitlab.com/LuisVallejom/proyecto.git`


`$ git remote -v`

`$ git push -u origin --all`

En el último paso, pedirá una autenticación con nuestro usuario y contraseña que anteriormente hemos creado en la cuenta de GitLab. Si la autenticación a sido correcta subirá el repositorio y tiene que aparecer en la consola que se ha subido correctamente.

Otra opción para subir el repositorio, en caso de no querer hacerlo con usuario y contraseña; es mediante SSH keys.




